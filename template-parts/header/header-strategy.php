<div class="nav-panel sticky-panel">
    <div class="container">
        <div class="holder">
            <div class="form-holder">
                <form action="#" class="select-form">
							<span class="fake-select-holder">
								<select class="fake-select">
									<option selected="selected">Institutional (CAMWX)</option>
									<option>Select option 2</option>
									<option>Select option 3</option>
									<option>Select option 4</option>
								</select>
							</span>
                </form>
            </div>
            <ul class="link-holder text-uppercase">
                <li>
                    <button class="link" onclick="window.print()" type="button"><i class="icon-print"></i>print</button>
                </li>
                <li>
                    <a href="#" class="link"><i class="icon-share"></i>shar</a>
                </li>
            </ul>
            <div class="anchor-box">
                <ul id="anchor-menu" class="slick-carousel-base text-uppercase">
                    <li class="base-slide">
                        <div data-menuanchor="slide1"><a href="#slide1">Overvie</a></div>
                    </li>
                    <li class="base-slide">
                        <div data-menuanchor="slide2"><a href="#slide2">Performan</a></div>
                    </li>
                    <li class="base-slide">
                        <div data-menuanchor="slide3"><a href="#slide3">Compositi</a></div>
                    </li>
                    <li class="base-slide">
                        <div data-menuanchor="slide4"><a href="#slide4">Comment</a></div>
                    </li>
                    <li class="base-slide">
                        <div data-menuanchor="slide5"><a href="#slide5">Cap gains &</a></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>