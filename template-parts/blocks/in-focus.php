<?php
$args = array(
    'numberposts' => 8,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' =>'',
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true
);

$recent_posts = wp_get_recent_posts( $args );
?>
<div id="focus" class="fp-block fp-auto-height">
    <section class="section-hero bg-stretch-mod bg-overlay-mod <?php echo $class; ?>" style="background-image: url('<?php echo $bg; ?>')">
        <div class="fade-bg"></div>
        <div class="fp-container">
            <div class="container">
            <div class="text-box text-white">
                <header class="section-header mod">
                    <h1 class="title text-uppercase">
                        <?php the_sub_field('title'); ?>
                    </h1>
                </header>
                <div class="text-holder display-xs-visible">
                    <?php the_sub_field('description'); ?>
                </div>
                <div class="latest-news">
                    <div class="post-holder">
                        <div class="post-box">
                            <?php
                            if( $recent_posts ) {
                                foreach ( $recent_posts as $i => $recent) {
                                    $a_class = $i == 0 ? 'new-post' : '';
                                    ?>
                                    <article class="post <?php echo $a_class; ?>">
                                        <div class="post-img-holder">
                                            <a href="<?php echo get_permalink($recent["ID"]); ?>">
                                                <div class="holder">
                                                    <?php echo get_the_post_thumbnail($recent["ID"], 'utheme-thumbnail'); ?>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="post-text-wrap">
                                            <div class="post-time-holder text-uppercase">
                                                <span>Category</span>
                                                <time datetime="2017-03-20"> - 20 Mar 2017</time>
                                            </div>
                                            <h2 class="post-title">
                                                <a href="<?php echo get_permalink($recent["ID"]); ?>"><?php echo $recent["post_title"]; ?></a>
                                            </h2>
                                            <div class="text-box">
                                                <div class="box">
                                                    <p><?php echo wp_strip_all_tags(get_the_excerpt($recent["ID"]), true); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="btn-box display-xs-hidden">
                        <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn btn-secondary text-uppercase">View All</a>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>