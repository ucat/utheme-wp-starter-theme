<div class="fp-block fp-auto-height">
    <section class="section-hero bg-stretch overlay <?php echo $class; ?>" style="background-image: url('<?php echo $bg; ?>')" >
        <div class="fp-container">
            <div class="container mod">
            <div class="text-box text-white">
                <header class="section-header mod">
                    <h1 class="title text-uppercase">
                        <?php the_sub_field('title'); ?>
                    </h1>
                </header>
                <div class="text-holder">
                    <?php the_sub_field('description'); ?>
                </div>
                <div class="video-box text-white">
                    <div class="u-yapi" data-url="<?php the_sub_field('youtube'); ?>">
                        <div class="holder" >
                            <div class="img-holder">
                                <img src="<?php the_sub_field('cover_image'); ?>" width="1216" height="608" alt="CAMBIAR INVESTORS">
                            </div>
                            <a href="#" class="play-btn">
                                <img src="<?php echo u_get_assets_uri('images/play-btn.svg'); ?>" width="96" height="96" alt="image description">
                            </a>
                        </div>
                        <div class="video-text text-white">
                            <div class="title text-uppercase">
                                <p>Explore Cambiar</p>
                            </div>
                            <div class="text">
                                <p>Video description lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-box display-xs-hidden">
                    <a href="#" class="btn btn-secondary text-uppercase">Learn More</a>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>