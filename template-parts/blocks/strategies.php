<?php
$columns = array(
    array(
        'title' => __('Product Name', 'utheme'),
        'key'   => 'title'
    ),
    array(
        'title' => __('Geography', 'utheme'),
        'key'   => 'geography'
    ),
    array(
        'title' => __('Category', 'utheme'),
        'key'   => 'category'
    ),
    array(
        'title' => __('Holdings', 'utheme'),
        'key'   => 'holdings'
    ),
    array(
        'title' => __('Weighting', 'utheme'),
        'key'   => 'weighting'
    ),
    array(
        'title' => __('Cap Range', 'utheme'),
        'key'   => 'cap_range'
    )
);

$groups = u_get_strategy_groups(true);
?>
<div class="fp-block fp-auto-height ">
    <section class="tab-section <?php echo $class; ?>">
        <div class="fp-container">
        <div class="container">
            <header class="section-header">
                <h1 class="title text-uppercase">
                    <?php the_sub_field('title'); ?>
                </h1>
            </header>
            <div class="text-holder">
                <?php the_sub_field('description'); ?>
            </div>
            <div class="tab-box">
                <div class="tabset-holder">
                    <ul class="tabset text-uppercase">
                        <?php $i = 0; foreach ( u_get_strategy_types() as $type_key => $type_name ): ?>
                        <li><a href="#tab-<?php echo $type_key; ?>" <?php $i === 0 ? 'class="active"' : ''; ?>><?php echo $type_name; ?></a></li>
                        <?php $i++; endforeach; ?>
                        <li><a href="#tab-3"><?php _e('Private Offering', 'utheme'); ?></a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <?php
                    foreach ( u_get_strategy_types() as $type_key => $type_name ):
                    ?>
                    <div id="tab-<?php echo $type_key; ?>">
                        <div class="tab-holder">
                            <div class="scroll-holder">
                                <div class="scroll-wrap">
                                    <table class="responsive-table table display">
                                        <thead>
                                        <tr>
                                            <?php foreach ($columns as $column ): ?>
                                            <th class="max-desktop no-sort"><?php echo $column['title']; ?></th>
                                            <?php if( $type_key == 'mutual_funds' && $column['key'] === 'title' ){ ?>
                                            <th class="min-tablet no-sort"><?php _e('Ticker', 'utheme'); ?></th>
                                            <th class="min-tablet">
                                                <div class="title"><?php _e('NAV ($)', 'utheme'); ?></div>
                                                <div class="subtitle">
                                                    <p><?php _e('as of 00/00/00', 'utheme'); ?></p>
                                                    <i class="icon-arrow-down display-xs-hidden"></i>
                                                </div>
                                            </th>
                                            <?php } ?>
                                            <?php endforeach; ?>
                                            <th class="min-tablet no-sort display-xs-visible"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        //$groups
                                        if( $groups ){
                                            foreach ($groups as $group_id => $group_title ){
                                                $strategies = u_get_strategies( array('type' => $type_key, 'group' => $group_id ) );
                                                if( !$strategies ) continue;
                                                $strategy = $strategies[0];
                                                ?>
                                            <tr>
                                                <?php $i = 0; foreach ($columns as $column ):
                                                    if( $i === 0 ): ?>
                                                    <td class="max-desktop">
                                                        <div class="mark-royal-blue">
                                                            <a href="<?php echo $strategy->get_permalink(); ?>"><?php echo $group_title; ?></a>
                                                        </div>
                                                    </td>

                                                    <?php if( $type_key == 'mutual_funds' ){ ?>
                                                            <td class="min-tablet">
                                                                <?php if( count($strategies) > 1 ){ ?>
                                                                <div class="sort-form">
																	<span class="fake-select-holder">
																		<select class="fake-select">
                                                                            <?php foreach ($strategies as $strtg){ ?>
																			<option value="<?php echo $strtg->id; ?>"><?php echo $strtg->ticker; ?></option>
                                                                            <?php } ?>
																		</select>
																	</span>
                                                                </div>
                                                                <?php }else{
                                                                echo $strategies[0]->ticker;
                                                                } ?>
                                                            </td>
                                                            <td class="min-tablet">
                                                                <?php echo $strategy->monthly_nav; ?>
                                                            </td>
                                                    <?php } ?>

                                                    <?php else: ?>
                                                    <td class="min-tablet"><?php echo $strategy->{$column['key']}; ?></td>
                                                    <?php endif;
                                                $i++; endforeach; ?>
                                                <td class="min-tablet display-xs-visible">
                                                    <a href="#" class="btn btn-secondary text-uppercase"><?php _e('Learn More', 'utheme'); ?></a>
                                                </td>
                                            </tr>
                                        <?php }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="btn-box display-xs-hidden">
                                <a href="<?php echo get_post_type_archive_link( 'strategy' ); ?>" class="btn btn-secondary text-uppercase">View All</a>
                            </div>

                            <?php if( $type_key == 'mutual_funds') { ?>
                            <div class="collapse-box">
                                <div class="title text-uppercase">Disclosures</div>
                                <div class="collapse-holder">
                                    <div class="text-box collapse">
                                        <?php echo apply_filters( 'the_content', get_option('u_strategy_disclosures') ); ?>
                                    </div>
                                    <a class="collapse-btn btn btn-secondary">
                                        <i class="icon-arrow-down"></i>
                                    </a>
                                </div>
                            </div>
                                <?php } ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <div id="tab-3">
                        <div class="tab-holder">
                            <div class="text-wrap">
                                <p>Cambiar provides a variety of investment solutions for high net worth private clients. These strategies offer increased flexibility and can be designed to meet specific investment objectives. For more information please contact us.</p>
                            </div>
                            <div class="wpcf7">
                                <form action="#" class="wpcf7-form" enctype="multipart/form-data">
                                    <div class="form-row">
                                        <div class="form-group">
														<span class="wpcf7-form-control-wrap text-field-required">
															<input type="text" name="text-field-required" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required text-field-class wpcf7-use-title-as-watermark" size="12" placeholder="First name"/>
														</span>
                                        </div>
                                        <div class="form-group">
														<span class="wpcf7-form-control-wrap text-field-required">
															<input type="text" name="text-field-required" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required text-field-class wpcf7-use-title-as-watermark" size="12" placeholder="Last name"/>
														</span>
                                        </div>
                                        <div class="form-group">
														<span class="wpcf7-form-control-wrap text-field-required">
															<input type="text" name="text-field-required" value="" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required text-field-class wpcf7-use-title-as-watermark" size="12" placeholder="City"/>
														</span>
                                        </div>
                                        <div class="form-group">
														<span class="wpcf7-form-control-wrap select wpcf7-validates-as-required">
															<span class="fake-select-holder">
																<select class="fake-select">
																	<option selected="selected">State</option>
																	<option>Select option 2</option>
																	<option>Select option 3</option>
																	<option>Select option 4</option>
																</select>
															</span>
														</span>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group">
														<span class="wpcf7-form-control-wrap Emailfield text-field-required">
															<input type="email" name="Emailfield" value="" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email email-class wpcf7-use-title-as-watermark" size="12" placeholder="Email address"/>
														</span>
                                        </div>
                                        <div class="form-group">
														<span class="wpcf7-form-control-wrap">
															<input type="tel" name="uphone" value="" class="wpcf7-form-control wpcf7-text" placeholder="Telephone Number"/>
														</span>
                                        </div>
                                        <div class="form-group grow">
														<span class="wpcf7-form-control-wrap textarea-required">
															<textarea name="textarea" class="wpcf7-form-control wpcf7-textarea textarea-class wpcf7-use-title-as-watermark" cols="10" rows="1" placeholder="I am interested in…"></textarea>
														</span>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group grow">
                                            <div class="btn-holder">
                                                <input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit submit-class btn btn-secondary" />
                                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>