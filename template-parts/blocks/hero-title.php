<div class="fp-block">
    <section class="section-hero bg-stretch bg-overlay <?php echo $class; ?>" style="background-image: url('<?php echo $bg; ?>')">
        <div class="fp-container full-height">
            <div class="container">
                <div class="text-box text-white">
                    <header class="section-header">
                        <h1 class="title text-uppercase"><?php the_sub_field('title'); ?></h1>
                    </header>
                    <div class="text-holder">
                        <?php the_sub_field('description'); ?>
                    </div>
                    <div class="anhor-holder display-xs-hidden">
                        <a href="<?php the_sub_field('button_link'); ?>" class="anhor-link">
                            <i class="icon-arrow-down-big"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>