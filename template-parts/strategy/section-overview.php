<?php
global $the_strategy;
?>
<div class="fp-block fp-auto-height" data-anchor="slide1">
    <section class="section-fund text-white" style="background-color: #455560;">
        <div class="container">
            <header class="section-header">
                <h1 class="title text-uppercase"><?php echo $the_strategy->group; ?></h1>
            </header>
            <div class="box-info">
                <ul class="list-info">
                    <li>
                        <h2 class="title h6"><?php _e('Net Asset Value', 'utheme'); ?></h2>
                        <div class="subtitle"><?php echo u_price_format($the_strategy->daily_price_nav); ?> <span class="small">(+0.00%)</span></div>
                        <div class="text-holder">
                            <p>as of <time datetime="<?php echo $the_strategy->daily_price_date; ?>"><?php echo $the_strategy->daily_price_date; ?></time></p>
                        </div>
                    </li>
                    <li>
                        <h2 class="title h6"><?php _e('YTD Return', 'utheme'); ?></h2>
                        <div class="subtitle"><?php echo u_percent_format($the_strategy->daily_price_ytd_return); ?></div>
                        <div class="text-holder">
                            <p>as of <time datetime="<?php echo $the_strategy->daily_price_date; ?>"><?php echo $the_strategy->daily_price_date; ?></time></p>
                        </div>
                    </li>
                    <li>
                        <h2 class="title h6"><?php _e('Total Net Assets', 'utheme'); ?></h2>
                        <div class="subtitle"><?php echo u_price_format($the_strategy->total_net_assets); ?></div>
                        <div class="text-holder">
                            <p>as of <time datetime="<?php echo $the_strategy->daily_price_date; ?>"><?php echo $the_strategy->daily_price_date; ?></time></p>
                        </div>
                    </li>
                    <li>
                        <h2 class="title h6"><?php _e('Morningstar Rating', 'utheme'); ?></h2>
                        <div class="subtitle">
                            <?php
                            if( $the_strategy->morningstar_rating ){
                                for ($st = 0; $st < $the_strategy->morningstar_rating; $st++ ){
                                    echo '<i class="icon-star"></i>';
                                }
                            }
                            ?>
                        </div>
                        <div class="text-holder display-xs-hidden">
                            <p><?php echo $the_strategy->morningstar_short_description; ?></p>
                        </div>
                    </li>
                </ul>
                <div class="collapse-wrap">
                    <div class="text-info">
                        <div class="collapse-holder mod">
                            <a class="collapse-btn mod text-uppercase">
                                <h2 class="title h6"><?php _e('Why Invest', 'utheme'); ?></h2>
                                <i class="icon-arrow-down"></i>
                            </a>
                            <div class="text-box collapse">
                                <div class="text-holder">
                                    <?php echo apply_filters( 'the_content', $the_strategy->why_invest ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lists-wrap">
                        <div class="lists-box">
                            <div class="list-item">
                                <div class="collapse-holder mod">
                                    <a class="collapse-btn mod text-uppercase">
                                        <h2 class="title h6"><?php _e('Key Facts', 'utheme'); ?></h2>
                                        <i class="icon-arrow-down"></i>
                                    </a>
                                    <div class="collapse">
                                        <dl class="facts-list list-group">
                                            <div>
                                                <dt><?php _e('CUSIP', 'utheme'); ?></dt>
                                                <dd><?php echo $the_strategy->cusip; ?></dd>
                                            </div>
                                            <div>
                                                <dt><?php _e('Inception Date', 'utheme'); ?></dt>
                                                <dd><?php echo $the_strategy->inception_date; ?></dd>
                                            </div>
                                            <div>
                                                <dt><?php _e('Minimum', 'utheme'); ?></dt>
                                                <dd><?php echo u_price_format($the_strategy->minimum_investment); ?></dd>
                                            </div>
                                            <div>
                                                <dt><?php _e('Gross Expense', 'utheme'); ?></dt>
                                                <dd><?php echo u_percent_format($the_strategy->gross_expense_ratio); ?></dd>
                                            </div>
                                            <div>
                                                <dt><?php _e('Net Expense Ratio', 'utheme'); ?></dt>
                                                <dd><?php echo u_percent_format($the_strategy->net_expense_ratio); ?></dd>
                                            </div>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $managers = $the_strategy->get_managers();
                            if( $managers && is_array($managers) ): ?>
                            <div class="list-item">
                                <div class="collapse-holder mod">
                                    <a class="collapse-btn mod text-uppercase">
                                        <h2 class="title h6"><?php _e('Managers', 'utheme'); ?></h2>
                                        <i class="icon-arrow-down"></i>
                                    </a>
                                    <div class="collapse">
                                        <ul class="manegers-list list-group">
                                            <?php
                                            foreach ($managers as $manager ):
                                                if (!isset($manager['id'])) continue;
                                                $the_manager = u_get_manager($manager['id']);
                                                ?>
                                                <li>
                                                    <div class="img-holder">
                                                        <?php echo $the_manager->get_thumbnail('utheme-manager-photo-small', ['alt' => $the_manager->get_title() ]); ?>
                                                    </div>
                                                    <div class="text-holder">
                                                        <div class="list-title">
                                                            <a href="<?php echo $the_manager->get_permalink(); ?>"><?php echo $the_manager->get_title(); ?></a>
                                                        </div>
                                                        <div class="text">
                                                            <p><?php echo $manager['description']; ?></p>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            <div class="list-item">
                                <div class="collapse-holder mod">
                                    <a class="collapse-btn mod text-uppercase">
                                        <h2 class="title h6"><?php _e('Literature', 'utheme'); ?></h2>
                                        <i class="icon-arrow-down"></i>
                                    </a>
                                    <div class="collapse">
                                        <ul class="literature-list list-group">
                                            <?php
                                            $additional_doc = !is_array($the_strategy->additional_doc) ? array() : $the_strategy->additional_doc;
                                            $documents_list = array_merge($the_strategy->get_documents_list(), $additional_doc);
                                            foreach ( $documents_list as $doc_id => $doc){ ?>
                                            <li>
                                                <a download="<?php echo $the_strategy->get_document_url($doc_id); ?>" href="<?php echo $the_strategy->get_document_url($doc_id); ?>" target="_blank" class="load-link"><?php echo $the_strategy->get_document_name($doc_id); ?> <i class="icon-download"></i></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="describe-box display-xs-hidden">
                <?php echo apply_filters( 'the_content', $the_strategy->morningstar_description ); ?>
            </div>
            <div class="anhor-holder display-xs-hidden">
                <a href="#focus" class="anhor-link">
                    <i class="icon-arrow-down-big"></i>
                </a>
            </div>
        </div>
    </section>
</div>