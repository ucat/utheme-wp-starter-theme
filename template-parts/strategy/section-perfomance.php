<?php
global $the_strategy;
?>
<div class="fp-block fp-auto-height" data-anchor="slide2">
    <section>
        <div class="container">
            <div class="container">
                <header class="section-header">
                    <h1 class="title text-uppercase">Performance</h1>
                </header>
                <div>
                    <div>
                        <div class="chart-header">
                            <h2 class="chart-title h6 text-uppercase">Total Returns at NAV</h2>
                            <div class="chart-subtitle">
                                <p>as of 00/00/00</p>
                            </div>
                        </div>
                        <ul class="tabset-table mod text-uppercase">
                            <li><a href="#table-1" class="active">Monthly</a></li>
                            <li><a href="#table-2">Quarterly</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div id="table-1">
                            <div></div>
                            <div class="table-holder">
                                <table class="display-table">
                                    <thead>
                                    <tr>
                                        <th class="no-sort"></th>
                                        <th class="no-sort">QTR</th>
                                        <th class="no-sort">YTD</th>
                                        <th class="no-sort">1 YR</th>
                                        <th class="no-sort">3 YR</th>
                                        <th class="no-sort">5 YR</th>
                                        <th class="no-sort">10 YR</th>
                                        <th class="no-sort">15 YR</th>
                                        <th class="no-sort">Incpt.</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>CAMWX</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                    </tr>
                                    <tr>
                                        <td>Russell 2500</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                    </tr>
                                    <tr>
                                        <td>Russell 2500V</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                        <td>0.0</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="table-2"></div>
                    </div>
                </div>
                <div class="text-box">
                    <p>The performance data quoted represents past performance and does not guarantee future results. The investment return and principal value of an investment will fluctuate so that an investor's shares, when redeemed, may be worth more or less than their original cost and current performance may be lower or higher than the performance quoted. Institutional Class Shares of the Fund commenced operations on November 3, 2005. As a result, the performance information provided for Institutional Class Shares incorporates the returns of Investor Class Shares of the Fund for periods before November 3, 2005. Institutional Class Shares would have substantially similar performance as Investor Class Shares because the shares are invested in the same portfolio of securities and the annual returns would generally differ only to the extent that total expenses of Institutional Class Shares are lower. The performance data quoted represents past performance and does not guarantee future results. The investment return and principal value of an investment will fluctuate so that an investor's shares, when redeemed, may be worth more or less than their original cost and current performance may be lower or higher than the performance quoted. Investor Share Class: Expense ratio is 1.11% (gross); 1.05% (net). Institutional Share Class: Expense ratio is 0.86% (gross); 0.80% (net). Cambiar Investors, LLC has contractually agreed to reduce fees and reimburse expenses in order to keep net operating expenses from exceeding 0.80% of the average daily net assets of each of the Fund’s share classes until September 1, 2017. S&P 500 Index is an unmanaged index compiled by Standard & Poor’s Corp. Russell 1000® Value Index measures the performance of those Russell 1000 companies with lower price-to-book ratios and lower forecasted growth values. Index returns do not reflect any management fees, transaction costs or expenses. Individuals cannot invest directly in an Index. For performance data current to the most recent month-end, please call 1-866-777-8227.</p>
                </div>
            </div>
        </div>
    </section>
</div>