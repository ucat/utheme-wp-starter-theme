<?php
global $the_strategy;
?>
<div class="fp-block fp-auto-height" data-anchor="slide3">
    <section>
        <div class="container">
            <div class="container">
                <header class="section-header">
                    <h1 class="title text-uppercase">Composition</h1>
                </header>
                <div>
                    <div>
                        <div>
                            <div></div>

                        </div>
                        <div id="chart_div"></div>
                    </div>
                    <div></div>
                </div>
                <div>
                    <div>
                        <div class="table-holder">
                            <table class="display-table">
                                <thead>
                                <tr>
                                    <th class="no-sort"></th>
                                    <th class="no-sort">Cambiar</th>
                                    <th class="no-sort">S&P 500</th>
                                    <th class="no-sort">Russell 1000V</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Price/Earnings F1Y</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>Price/Book</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>Debt/Equity</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>EPS Growth</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>Market Cap Wtd Avg</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>Market Cap Median</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div>
                        <div class="table-holder">
                            <table class="display-table">
                                <thead>
                                <tr>
                                    <th class="no-sort"></th>
                                    <th class="no-sort">Cambiar</th>
                                    <th class="no-sort">S&P 500</th>
                                    <th class="no-sort">Russell 1000V</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Alpha</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>Beta</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>R-Squared</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>Sharpe Ratio </td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                <tr>
                                    <td>Standard Deviation</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                    <td>0.0</td>
                                </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="4">* Three year</td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <div></div>
            </div>
        </div>
    </section>
</div>