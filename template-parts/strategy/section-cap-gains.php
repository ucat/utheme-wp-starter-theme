<?php
global $the_strategy;

$columns = U_Meta_Box_Strategy_Data::get_cap_gains_columns();
unset($columns['action']);
?>
<div class="fp-block fp-auto-height" data-anchor="slide5">
    <section class="section-gains">
        <div class="container">
            <header class="section-header">
                <h1 class="title text-uppercase">Cap Gains & Income</h1>
            </header>
            <form action="#" class="datepicker-form">
                <div class="datepicker-range">
                    <div class="datepicker-holder" data-text="From:">
                        <input type="text" data-datepicker-from=''>
                        <i class="icon-date"></i>
                    </div>
                    <div class="datepicker-holder" data-text="To:">
                        <input type="text" data-datepicker-to=''>
                        <i class="icon-date"></i>
                    </div>
                </div>
            </form>
            <div class="table-holder">
                <table class="display-table">
                    <thead>
                    <tr>
                        <?php foreach ($columns as $c_i => $c_d ){ ?>
                        <th class="no-sort"><?php echo $c_d['label']; ?></th>
                        <?php } ?>
                    </tr>

                    </thead>
                    <tbody>
                    <?php foreach ($the_strategy->cap_gains as $cap_gains ){ ?>
                        <tr>
                            <?php foreach ($columns as $c_i => $c_d ){ ?>
                                <td><?php echo $c_i === 'year' ? $cap_gains[$c_i] : u_price_format($cap_gains[$c_i]); ?></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4"><?php echo apply_filters( 'the_content', $the_strategy->cap_gains_description ); ?></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="collapse-box">
                <div class="title text-uppercase">Disclosures</div>
                <div class="collapse-holder">
                    <div class="text-box collapse">
                        <?php echo apply_filters( 'the_content', get_option('u_strategy_disclosures') ); ?>
                    </div>
                    <a class="collapse-btn btn btn-secondary">
                        <i class="icon-arrow-down"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
</div>