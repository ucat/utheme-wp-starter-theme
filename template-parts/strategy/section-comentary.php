<?php
global $the_strategy;
?>
<div class="fp-block fp-auto-height" data-anchor="slide4">
    <section class="section-commentary">
        <div class="container">
            <header class="section-header">
                <h1 class="title text-uppercase">Commentary</h1>
            </header>
            <div class="content-wrap">
                <div id="content">
                    <div class="video-box mod">
                        <div class="holder">
                            <a href="#" class="play-btn">
                                <img src="assets/images/play-btn.svg" width="96" height="96" alt="image description">
                            </a>
                        </div>
                        <div class="video-text">
                            <div class="title text-uppercase">
                                <p>Portfolio Review Video or Podcast</p>
                            </div>
                            <div class="text">
                                <p>Video description lorem ipsum dolor sit amet.</p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio-box collapse-holder mod">
                        <h2 class="portfolio-box-title text-uppercase collapse-btn mod">Portfolio Review<i class="icon-arrow-down"></i></h2>
                        <div class="collapse">
                            <ul class="portfolio-list">
                                <li>
                                    <div class="portfolio-text-holder">
                                        <h3 class="portfolio-list-title text-uppercase">Lorem Ipsum Dolor</h3>
                                        <div class="portfolio-list-text">
                                            <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo<a href="#">inline link.</a></p>
                                        </div>
                                    </div>
                                    <div class="portfolio-list-img-wrap">
                                        <div class="holder">
                                            <figure class="img-holder">
                                                <figcaption class="img-caption text-uppercase">Figure 1</figcaption>
                                                <div>
                                                    <img src="assets/images/figure-1.png" width="512" height="276" alt="image description">
                                                </div>
                                            </figure>
                                            <div class="btn-holder">
                                                <a href="assets/images/figure-1.png" class="lightbox btn btn-secondary text-uppercase" type="button"><i class="icon-zoom"></i>zoom</a>
                                            </div>
                                            <div class="img-text-holder">
                                                <p>Source: Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="portfolio-text-holder">
                                        <h3 class="portfolio-list-title text-uppercase">Sit Amet</h3>
                                        <div class="portfolio-list-text">
                                            <p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                                            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
                                        </div>
                                    </div>
                                    <div class="portfolio-list-img-wrap">
                                        <div class="holder">
                                            <figure class="img-holder">
                                                <figcaption class="img-caption text-uppercase">Figure 2</figcaption>
                                                <div>
                                                    <img src="assets/images/figure-2.png" width="510" height="276" alt="image description">
                                                </div>
                                            </figure>
                                            <div class="btn-holder">
                                                <a href="assets/images/figure-2.png" class="lightbox btn btn-secondary text-uppercase" type="button"><i class="icon-zoom"></i>zoom</a>
                                            </div>
                                            <div class="img-text-holder">
                                                <p>Source: Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <aside id="sidebar">
                    <div class="post-widget widget collapse-holder mod">
                        <header class="widget-header">
                            <h2 class="title text-uppercase collapse-btn mod">Related Insights<i class="icon-arrow-down"></i></h2>
                        </header>
                        <div class="collapse">
                            <div class="widget-content">
                                <article class="post">
                                    <div class="post-text-wrap">
                                        <div class="post-time-holder text-uppercase">
                                            <span>Category</span>
                                            <time datetime="2017-03-20"> - DATE</time>
                                        </div>
                                        <h2 class="post-title">
                                            <a href="#">Sample Video or Podcast Lorem Ipsum</a>
                                        </h2>
                                    </div>
                                    <div class="post-img-holder"></div>
                                </article>
                                <article class="post">
                                    <div class="post-text-wrap">
                                        <div class="post-time-holder text-uppercase">
                                            <span>Category</span>
                                            <time datetime="2017-03-20"> - DATE</time>
                                        </div>
                                        <h2 class="post-title">
                                            <a href="#">Sample White Paper Lorem Ipsum Dolor</a>
                                        </h2>
                                    </div>
                                    <div class="post-img-holder"></div>
                                </article>
                                <article class="post">
                                    <div class="post-text-wrap">
                                        <div class="post-time-holder text-uppercase">
                                            <span>Category</span>
                                            <time datetime="2017-03-20"> - DATE</time>
                                        </div>
                                        <h2 class="post-title">
                                            <a href="#">Sample Article Lorem Ipsum Dolor Sit Amet</a>
                                        </h2>
                                    </div>
                                    <div class="post-img-holder"></div>
                                </article>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
</div>