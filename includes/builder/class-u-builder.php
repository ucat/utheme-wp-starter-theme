<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Strategy
 *
 *
 * @class     U_Builder
 * @version   1.0.0
 * @package   U_Theme/Classes
 * @category  Class
 * @author    Elena Zhyvohliad
 */
class U_Builder{

    public $version = '1.0.0';


    /**
     * Constructor.
     */
    public function __construct() {

        if(isset($_GET['umarkup'])){
            $this->init();
        }
    }

    private function init(){

        add_action('wp_enqueue_scripts', array($this, 'add_styles'));
        add_action('wp_enqueue_scripts', array($this, 'add_scripts'));
        add_action('the_content', array($this, 'the_content'));
        add_action('wp_footer', array($this, 'output_app'));
        add_filter('show_admin_bar', '__return_false');
    }

    /**
     * Enqueue styles.
     */
    public function add_styles() {
        if( is_page() ) {
            //wp_enqueue_style('ubuilder-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', [], $this->version);
            wp_enqueue_style('ubuilder-style', u_get_assets_uri('builder/css/app-styles.css'), array('dashicons'), $this->version);
        }
    }


    /**
     * Enqueue scripts.
     */
    public function add_scripts() {
        if( is_page() ) {
            $depth = ['jquery', 'ubuilder-bootstrap'];

            wp_register_script('ubuilder-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', [], $this->version, true);
            wp_register_script('ubuilder-scripts', u_get_assets_uri('builder/js/app.js'), $depth, $this->version, true);
            wp_enqueue_script('ubuilder-scripts');
        }
    }

    public function the_content($content){
        return '<u-app></u-app>';
    }


    public function output_app(){
        if( is_page() ) {
            include 'views/html-builder-sidebar.php';
            //include 'views/html-builder-sidebar.php';
        }
    }


}

return new U_Builder();