<div class="u-att-group setting-group">
    <a href="#" class="u-remove-attr"><span class="dashicons dashicons-dismiss"></span></a>
    <div class="setting">
        <span>Name</span>
        <input type="text" class="u-att-name u-tag-att">
    </div>
    <div class="setting">
        <span>Value</span>
        <input type="text" class="u-att-val u-tag-att">
    </div>
</div>