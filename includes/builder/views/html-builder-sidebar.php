<u-app-sidebar>
    <u-header>
        <ul class="u-nav nav-tabs flex-container">
            <li class="active">
                <a href="#u-tab-general" data-toggle="tab" aria-expanded="true">General</a>
            </li>
            <li>
                <a href="#u-tab-attributes" data-toggle="tab" aria-expanded="true">Attr</a>
            </li>
            <li>
                <a href="#u-tab-styles" data-toggle="tab" aria-expanded="true">Styles</a>
            </li>
            <li>
                <a href="#u-tab-dom" data-toggle="tab" aria-expanded="true">DOM</a>
            </li>
        </ul>
        <div class="u-breadcrumb-wrapper">
            <ol class="breadcrumb">
                <li><a href="#">Make</a>
                </li>
                <li><a href="#">Forms</a>
                </li>
                <li class="active">Form Elements</li>
            </ol>
        </div>
    </u-header>
    <u-content>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="u-tab-general">

                <div class="settings">
                    <div class="setting">
                        <span>Tag name</span>
                        <input type="text" id="u-tag-name">
                    </div>
                    <div class="setting">
                        <span>Content</span>
                        <textarea id="u-tag-content"></textarea>
                    </div>
                    <div class="setting">
                        <span>Lock Element</span>
                        <label class="switch pull-right">
                            <input type="checkbox" class="switch-input" checked="">
                            <span class="switch-label" data-on="On" data-off="Off"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <p class="setting-info">Lorem ipsum dolor sit amet consectetuer.</p>
                    </div>

                </div>
            </div>
            <div class="tab-pane fade" id="u-tab-attributes">

                <div class="settings">
                    <div class="title">GENERAL ATTRIBUTES</div>
                    <div id="u-general-attributes">
                        <div class="setting">
                            <span>Class</span>
                            <input type="text" class="u-tag-att" u-attr="class">
                        </div>
                        <div class="setting">
                            <span>ID</span>
                            <input type="text" class="u-tag-att" u-attr="id">
                        </div>
                    </div>

                    <div class="title">ADDITIONAL ATTRIBUTES</div>
                    <div id="u-additional-attributes">
                        <?php include "tmpl-additiona-attribute-row.php";?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="u-tab-styles">

                <div class="settings">
                    <div class="title">BACKGROUND</div>
                    <div id="u-general-attributes">
                        <div class="setting">
                            <input type="text" id="u-tag-background">
                        </div>
                        <div class="setting">
                            <button id="u-select-file">Select image</button>
                        </div>
                    </div>

                    <div class="title">CUSTOM CSS</div>
                    <div class="setting">
                        <textarea id="u-custom-css"></textarea>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="u-tab-dom">
                <div class="settings">
                </div>
            </div>
        </div>
    </u-content>
    <u-footer>
        <div class="item-footer cleatfix">
            <div class="footer-actions">
                <button id="u-add-tag">Add tag</button>
                <button id="u-add-attr-row">Add attribute</button>
                <button id="u-remove-tag" style="float: right;">Remove tag</button>
            </div>
        </div>
    </u-footer>

</u-app-sidebar>
<script type="text/template" id="utmpl-additiona-attribute-row">
    <?php include "tmpl-additiona-attribute-row.php";?>
</script>