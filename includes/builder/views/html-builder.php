<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Strategy
 *
 *
 * @class     U_Builder
 * @version   1.0.0
 * @package   U_Theme/Classes
 * @category  Class
 * @author    Elena Zhyvohliad
 */
class U_Builder{


    /**
     * Is meta boxes saved once?
     *
     * @var boolean
     */
    private static $saved_meta_boxes = false;

    /**
     * Meta box error messages.
     *
     * @var array
     */
    public static $meta_box_errors  = array();

    /**
     * Constructor.
     */
    public function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 30 );
        add_action( 'save_post', array( $this, 'save_meta_boxes' ), 1, 2 );


        //add_action( 'utheme_process_strategy_meta', 'U_Meta_Box_Strategy_Data::save', 40, 2 );

        // Error handling (for showing errors from meta boxes on next page load)
        add_action( 'admin_notices', array( $this, 'output_errors' ) );
        add_action( 'shutdown', array( $this, 'save_errors' ) );
    }

    /**
     * Add an error message.
     * @param string $text
     */
    public static function add_error( $text ) {
        self::$meta_box_errors[] = $text;
    }

    /**
     * Save errors to an option.
     */
    public function save_errors() {
        update_option( 'utheme_meta_box_errors', self::$meta_box_errors );
    }

    /**
     * Show any stored error messages.
     */
    public function output_errors() {
        $errors = maybe_unserialize( get_option( 'utheme_meta_box_errors' ) );

        if ( ! empty( $errors ) ) {

            echo '<div id="utheme_errors" class="error notice is-dismissible">';

            foreach ( $errors as $error ) {
                echo '<p>' . wp_kses_post( $error ) . '</p>';
            }

            echo '</div>';

            // Clear
            delete_option( 'utheme_meta_box_errors' );
        }
    }


    /**
     * Add Meta boxes.
     */
    public function add_meta_boxes() {

        foreach ( array('page') as $type ) {
            add_meta_box( 'utheme-page-builder', __( 'Page builder', 'utheme' ), array($this, 'output'), $type, 'advanced', 'high' );

        }

    }

    public function output (){
        
    }


    /**
     * Check if we're saving, the trigger an action based on the post type.
     *
     * @param  int $post_id
     * @param  object $post
     */
    public function save_meta_boxes( $post_id, $post ) {
        // $post_id and $post are required
        if ( empty( $post_id ) || empty( $post ) || self::$saved_meta_boxes ) {
            return;
        }

        // Dont' save meta boxes for revisions or autosaves
        if ( defined( 'DOING_AUTOSAVE' ) || is_int( wp_is_post_revision( $post ) ) || is_int( wp_is_post_autosave( $post ) ) ) {
            return;
        }

        // Check the nonce
        if ( empty( $_POST['utheme_builder_nonce'] ) || ! wp_verify_nonce( $_POST['utheme_builder_nonce'], 'utheme_save_builder' ) ) {
            return;
        }

        // Check the post being saved == the $post_id to prevent triggering this call for other save_post events
        if ( empty( $_POST['post_ID'] ) || $_POST['post_ID'] != $post_id ) {
            return;
        }

        // Check user has permission to edit
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }

        self::$saved_meta_boxes = true;

        // Check the post type
        if ( $post->post_type == 'strategy' ) {
            do_action( 'utheme_process_'.$post->post_type.'_meta', $post_id, $post );
        }

    }

}

return new U_Builder();