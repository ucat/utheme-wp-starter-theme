<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor hero widget.
 *
 * Elementor widget that displays hero section.
 *
 * @since 1.0.0
 */
class U_Elementor_Widget_Hero extends \Elementor\Widget_Base {
    public function get_name() {
        return 'hero';
    }

    public function get_title() {
        return __( 'Hero', 'utheme' );
    }

    public function get_icon() {
        return 'fa fa-code';
    }

    public function get_categories() {
        return [ 'utheme-elements' ];
    }

    protected function _register_controls() {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __( 'Content', 'plugin-name' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => __( 'Title', 'plugin-name' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text'
            ]
        );

        $this->add_control(
            'description',
            [
                'label' => __( 'Description', 'plugin-name' ),
                'type' => \Elementor\Controls_Manager::WYSIWYG,
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings_for_display();

        $description = $settings['description'];
        $title       = $settings['title'];

        require 'view/hero.php';

    }
}
