<section class="section-hero bg-stretch overlay">
        <div class="container">
            <div class="text-box text-white">
                <header class="section-header">
                    <h1 class="title text-uppercase"><?php echo $title; ?></h1>
                </header>
                <div class="text-holder">
                    <?php echo $description; ?>
                </div>
                <div class="anhor-holder display-xs-hidden">
                    <a href="#focus" class="anhor-link">
                        <i class="icon-arrow-down-big"></i>
                    </a>
                </div>
            </div>
        </div>
</section>