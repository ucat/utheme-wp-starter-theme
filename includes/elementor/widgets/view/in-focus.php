<section class="section-hero bg-stretch-mod overlay-mod">
                    <div class="fade-bg"></div>
                    <div class="container">
                        <div class="text-box text-white">
                            <header class="section-header mod">
                                <h1 class="title text-uppercase">
                                    <span class="display-xs-visible">Investing with the Courage of Our Convictions</span>
                                    <span class="display-xs-hidden">In Focus</span>
                                </h1>
                            </header>
                            <div class="text-holder display-xs-visible">
                                <p>We think deeply and differently to find the best ideas for our investors.</p>
                            </div>
                            <div class="latest-news">
                                <div class="post-holder">
                                    <div class="post-box">
                                        <?php
                                        foreach( $recent_posts as $i => $recent ){
                                            $a_class = $i == 0 ? 'new-post' : '';
                                            ?>
                                            <article class="<?php echo $a_class; ?>">
                                                <div class="img-holder">
                                                    <a href="<?php echo get_permalink($recent["ID"]); ?>">
                                                        <div class="holder">
                                                            <?php echo get_the_post_thumbnail($recent["ID"], 'utheme-thumbnail'); ?>
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="text-wrap">
                                                    <div class="time-holder text-uppercase">
                                                        <span>Category</span>
                                                        <time datetime="2017-03-20"> - 20 Mar 2017</time>
                                                    </div>
                                                    <h2 class="post-title">
                                                        <a href="<?php echo get_permalink($recent["ID"]); ?>"><?php echo $recent["post_title"]; ?></a>
                                                    </h2>
                                                    <div class="text-box">
                                                        <div class="box">
                                                            <p><?php echo wp_strip_all_tags( get_the_excerpt($recent["ID"]), true ); ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="btn-box display-xs-hidden">
                                    <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="btn btn-secondary text-uppercase">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>