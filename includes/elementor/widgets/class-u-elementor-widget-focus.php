<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Elementor widget.
 *
 * Elementor widget that displays section.
 *
 * @since 1.0.0
 */
class U_Elementor_Widget_Focus extends \Elementor\Widget_Base {
    public function get_name() {
        return 'focus';
    }

    public function get_title() {
        return __( 'In Focus', 'utheme' );
    }

    public function get_icon() {
        return 'fa fa-code';
    }

    public function get_categories() {
        return [ 'utheme-elements' ];
    }

    protected function _register_controls() {



    }

    protected function render() {

        $args = array(
            'numberposts' => 8,
            'offset' => 0,
            'category' => 0,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' =>'',
            'post_type' => 'post',
            'post_status' => 'publish',
            'suppress_filters' => true
        );

        $recent_posts = wp_get_recent_posts( $args );
        require 'view/in-focus.php';

    }
}
