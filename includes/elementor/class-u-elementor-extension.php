<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

final class U_Elementor_Extension {

    public function __construct() {
        add_action( 'init', array( $this, 'init' ) );
        add_action( 'elementor/elements/categories_registered', array( $this, 'add_widget_categories' ));

        // Register widgets
        add_action( 'elementor/widgets/widgets_registered', array( $this, 'register_widgets' ) );
    }
    public function init() {
        if ( ! did_action( 'elementor/loaded' ) ) {
            return;
        }

        $this->includes();

    }

    public function includes() {
        include_once( 'widgets/class-u-elementor-widget-hero.php' );
        include_once( 'widgets/class-u-elementor-widget-focus.php' );
        //include_once( 'controls/class-u-elementor-widget-hero.php' );
    }

    public function add_widget_categories( $elements_manager ){
        $elements_manager->add_category(
            'utheme-elements',
            [
                'title' => __( 'Theme Elements', 'plugin-name' ),
                'icon' => 'fa fa-plug',
            ]
        );
    }

    public function register_widgets() {

        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new U_Elementor_Widget_Hero() );
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new U_Elementor_Widget_Focus() );

    }

}
return new U_Elementor_Extension();