<?php
/**
 * Load assets
 *
 * @author      uCAT
 * @package     U_Theme/Classes/Assets
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'U_Frontend_Assets' ) ) :

/**
 * U_Frontend_Assets Class.
 */
class U_Frontend_Assets{

	/**
	 * Hook in tabs.
	 */
	public function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'add_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts' ) );
	}

	/**
	 * Enqueue styles.
	 */
	public function add_styles() {
        // Theme stylesheet.
        #wp_enqueue_style( 'utheme-style', get_stylesheet_uri() );

        wp_enqueue_style( 'utheme-general-style', u_get_assets_uri( 'css/style.css' ), array(), U_THEME_VERSION );
        wp_enqueue_style( 'utheme-frontend', u_get_assets_uri( 'css/frontend.css' ), array(), U_THEME_VERSION );
	}


	/**
	 * Enqueue scripts.
	 */
	public function add_scripts() {
        wp_enqueue_script( 'utheme-scripts', u_get_assets_uri( 'js/jquery.main.js' ), array('jquery'), U_THEME_VERSION );
        wp_enqueue_script( 'utheme-frontend-scripts', u_get_assets_uri( 'js/frontend.js' ), array('jquery'), rand() );
        wp_enqueue_script( 'utheme-youtube-scripts', u_get_assets_uri( 'js/youtube.api.js' ), array('jquery'), rand() );
	}

}

endif;

return new U_Frontend_Assets();
