<?php
/**
 *
 * @author      uCAT
 * @category    Admin
 * @package     U_Theme/Admin/
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'U_Admin_Import_Strategies', false ) ) :

/**
 * U_Admin_Import_Strategies Class.
 */
class U_Admin_Import {

    public static function output(){
        if ( ! current_user_can( 'import' ) ) {
            wp_die( __( 'Sorry, you are not allowed to import content.' ) );
        }
        include 'views/html-admin-page-import.php';
    }
}

endif;
