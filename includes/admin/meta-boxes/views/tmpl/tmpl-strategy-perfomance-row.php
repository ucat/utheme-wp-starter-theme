<script type="text/template" id="tmpl-strategy-<?php echo $meta_key; ?>-row">
    <tr>
        <?php foreach( $columns as $key => $col ){ ?>
            <td class="column-<?php echo $key; ?>">
                <?php
                switch ( $key ) {
                    case 'name':
                        ?>
                        <input name="_<?php echo $meta_key; ?>_<?php echo $key; ?>[]" type="text" class="form-control" value="">
                        <?php
                        break;
                    case 'action':
                        ?>
                        <a href="#" class="delete"><?php _e('Delete', 'utheme'); ?></a>
                        <?php
                        break;
                    default:
                        ?>
                        <input name="_<?php echo $meta_key; ?>_<?php echo $key; ?>[]" type="number" min="0" step="0.01" class="form-control" value="">
                        <?php
                        break;
                }
                ?>
            </td>
            <?php
        } ?>
    </tr>
</script>