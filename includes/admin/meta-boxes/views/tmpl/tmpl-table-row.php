<script type="text/template" id="<?php echo $tpl_id; ?>">
    <tr>
        <?php foreach( $columns as $key => $col ){ ?>
            <td class="column-<?php echo $key; ?>">
                <?php
                $placeholder = isset($col['placeholder']) ? 'placeholder="'.$col['placeholder'].'"' : '';
                switch ( $col['type'] ) {
                    case 'text':
                        ?>
                        <input name="_<?php echo $tab_id; ?>_<?php echo $key; ?>[]" type="text" <?php echo $placeholder; ?> class="form-control" value="">
                        <?php
                        break;
                    case 'textarea':
                        ?>
                        <textarea name="_<?php echo $tab_id; ?>_<?php echo $key; ?>[]" class="form-control" ></textarea>
                        <?php
                        break;
                    case 'select':
                        ?>
                        <select name="_<?php echo $tab_id; ?>_<?php echo $key; ?>[]" class="form-control">
                            <?php
                            if( $col['options'] && is_array($col['options']) ){
                                foreach ( $col['options'] as $v => $l ){
                                    ?>
                                    <option value="<?php echo $v; ?>" ><?php echo $l; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <?php
                        break;
                    case 'number':
                        ?>
                        <input name="_<?php echo $tab_id; ?>_<?php echo $key; ?>[]" type="number" step="0.01" min="0" <?php echo $placeholder; ?> class="form-control" value="">
                        <?php
                        break;
                    case 'upload_file':
                        ?>
                        <a href="#" class="button upload_file_button" data-choose="<?php _e('Choose file', 'utheme'); ?>" data-update="<?php _e('Insert file URL', 'utheme'); ?>">
                            <?php _e('Choose&nbsp;file', 'utheme'); ?>
                        </a>
                        <?php
                        break;
                    case 'action':
                        ?>
                        <a href="#" class="delete"><?php _e('Delete', 'utheme'); ?></a>
                        <?php
                        break;
                }
                ?>
            </td>
            <?php
        } ?>
    </tr>
</script>