<?php
$columns = U_Meta_Box_Strategy_Data::get_perfomance_columns();
$columns['action'] = '&nbsp;';

$prf_type = $tab_id == 'perfomance_monthly' ? 'monthly' : 'quarterly';
$meta_key = 'additional_'.$tab_id;
?>
    <table class="wp-list-table widefat striped u-data-table" id="<?php echo $meta_key; ?>_table">
        <thead>
        <tr>
            <?php foreach( $columns as $key => $label ){
                if( $key === 'qtr' && $prf_type == 'monthly'){
                    $label = __( 'MTH', 'utheme' );
                }
                ?>
                <th  class="column-<?php echo $key; ?>" ><?php echo $label; ?></th>
                <?php
            } ?>
        </tr>
        </thead>
        <tbody>
        <tr>
            <?php foreach( $columns as $key => $col ){ ?>
                <td class="column-<?php echo $key; ?>">
                    <?php
                    if( $key === 'name' ){
                        echo !empty($the_strategy->ticker) ? $the_strategy->ticker : __('Curent strategy', 'utheme');
                    }elseif( $key === 'action' ){
                    }else{
                        ?>
                        <input id="<?php echo $prf_type; ?>_<?php echo $key; ?>" name="_<?php echo $prf_type; ?>[<?php echo $key; ?>]" type="number" min="0" step="0.01" class="form-control" value="<?php echo $the_strategy->{$prf_type.'_'.$key}; ?>">
                        <?php
                    }
                    ?>
                </td>
                <?php
            } ?>
        </tr>
        <?php
        $additional_perfomance =  $the_strategy->{$meta_key} && is_array($the_strategy->{$meta_key}) ? $the_strategy->{$meta_key} : false;
        if( $additional_perfomance ){
        foreach ($additional_perfomance as $item_index=> $item){
        ?>
        <tr>
            <?php foreach( $columns as $key => $col ){ ?>
                <td class="column-<?php echo $key; ?>">
                    <?php
                    switch ( $key ) {
                        case 'name':
                            ?>
                            <input name="_<?php echo $meta_key; ?>_<?php echo $key; ?>[]" type="text" class="form-control" value="<?php echo $item[$key]; ?>">
                            <?php
                            break;
                        case 'action':
                            ?>
                            <a href="#" class="delete"><?php _e('Delete', 'utheme'); ?></a>
                            <?php
                            break;
                        default:
                            ?>
                            <input name="_<?php echo $meta_key; ?>_<?php echo $key; ?>[]" type="number" min="0" step="0.01" class="form-control" value="<?php echo $item[$key]; ?>">
                            <?php
                            break;
                    }
                    ?>
                </td>
                <?php
            } ?>
        </tr>
        <?php }
        } ?>
        </tbody>
    </table>
    <div class="clear"></div>

<div class="toolbar">
    <button type="button" data-tmpl="#tmpl-strategy-<?php echo $meta_key; ?>-row"  data-container="#<?php echo $meta_key; ?>_table" class="button add_document_row"><?php _e('Add row', 'utheme'); ?></button>
</div>

<?php
include 'tmpl/tmpl-strategy-perfomance-row.php';