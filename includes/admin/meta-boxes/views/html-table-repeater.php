    <table class="wp-list-table widefat striped u-data-table" id="<?php echo $table_id; ?>">
        <thead>
        <tr>
            <?php foreach( $columns as $key => $col ){
                ?>
                <th  class="column-<?php echo $key; ?>" ><?php echo $col['label']; ?></th>
                <?php
            } ?>
        </tr>
        </thead>
        <tbody>

        <?php
        if( $fields && is_array($fields) ){
        foreach ($fields as $field_index=> $field){
        ?>
        <tr>
            <?php foreach( $columns as $key => $col ){ ?>
                <td class="column-<?php echo $key; ?>">
                    <?php
                    $placeholder = isset($col['placeholder']) ? 'placeholder="'.$col['placeholder'].'"' : '';
                    switch ( $col['type'] ) {
                        case 'text':
                            ?>
                            <input name="_<?php echo $tab_id; ?>_<?php echo $key; ?>[]" type="text" <?php echo $placeholder; ?> class="form-control" value="<?php echo isset($field[$key]) ? $field[$key] : ''; ?>">
                            <?php
                            break;
                        case 'textarea':
                            ?>
                            <textarea name="_<?php echo $tab_id; ?>_<?php echo $key; ?>[]" class="form-control" ><?php echo isset($field[$key]) ? $field[$key] : ''; ?></textarea>
                            <?php
                            break;
                        case 'select':
                            ?>
                            <select name="_<?php echo $tab_id; ?>_<?php echo $key; ?>[]" class="form-control">
                                <?php
                                $fv = isset($field[$key]) ? $field[$key] : '';
                                if( $col['options'] && is_array($col['options']) ){
                                    foreach ( $col['options'] as $v => $l ){
                                        ?>
                                        <option value="<?php echo $v; ?>" <?php selected($v, $fv, true); ?> ><?php echo $l; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                            <?php
                            break;
                        case 'number':
                            ?>
                            <input name="_<?php echo $tab_id; ?>_<?php echo $key; ?>[]" type="number" step="0.01" min="0" <?php echo $placeholder; ?> class="form-control" value="<?php echo isset($field[$key]) ? $field[$key] : ''; ?>">
                            <?php
                            break;
                        case 'upload_file':
                            ?>
                            <a href="#" class="button upload_file_button" data-choose="<?php _e('Choose file', 'utheme'); ?>" data-update="<?php _e('Insert file URL', 'utheme'); ?>">
                                <?php _e('Choose&nbsp;file', 'utheme'); ?>
                            </a>
                            <?php
                            break;
                        case 'action':
                            ?>
                            <a href="#" class="delete"><?php _e('Delete', 'utheme'); ?></a>
                            <?php
                            break;
                    }
                    ?>
                </td>
                <?php
            } ?>
        </tr>
        <?php }
        } ?>
        </tbody>
    </table>
    <div class="clear"></div>

<div class="toolbar">
    <button type="button" data-tmpl="#tmpl-strategy-<?php echo $table_id; ?>-row"  data-container="#<?php echo $table_id; ?>" class="button add_table_row"><?php _e('Add row', 'utheme'); ?></button>
</div>

<?php
$tpl_id = 'tmpl-strategy-'.$table_id.'-row';
include 'tmpl/tmpl-table-row.php';