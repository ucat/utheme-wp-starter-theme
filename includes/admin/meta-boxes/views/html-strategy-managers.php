<?php
$columns = [
    'manager' => [
        'label'       => __( 'Manager', 'utheme' ),
        'type'        => 'select',
        'options'     => u_get_managers_list()
    ],
    'description' => [
        'label'       => __( 'Description', 'utheme' )
    ],
    'photo_url' => [
        'label'       => __( 'Photo', 'utheme' )
    ],
    'upload_file' => [
        'label'       => '&nbsp;'
    ],
    'action' => [
        'label'       => '&nbsp;'
    ],
];
?>
    <table class="wp-list-table widefat striped u-data-table" id="managers_table">
        <thead>
        <tr>
            <?php foreach( $columns as $key => $col ){
                ?>
                <th  class="column-<?php echo $key; ?>" ><?php echo $col['label']; ?></th>
                <?php
            } ?>
        </tr>
        </thead>
        <tbody>
        <?php
        if( $the_strategy->get_managers() && is_array($the_strategy->get_managers()) ){
        foreach ($the_strategy->get_managers() as $index=> $manager){
        ?>
        <tr>
            <?php foreach( $columns as $key => $col ){ ?>
                <td class="column-<?php echo $key; ?>">
                    <?php
                    switch ( $key ) {
                        case 'name':
                            ?>
                            <input name="_managers_<?php echo $key; ?>[]" type="text" placeholder="<?php echo $col['placeholder']; ?>" class="form-control" value="<?php echo $manager[$key]; ?>">
                            <?php
                            break;
                        case 'description':
                            ?>
                            <textarea name="_managers_<?php echo $key; ?>[]"><?php echo $manager[$key]; ?></textarea>
                            <?php
                            break;
                        case 'photo_url':
                            ?>
                            <img src="<?php echo $manager[$key]; ?>" alt="" style="<?php echo $manager[$key] ? '' : 'display: none;'; ?>" width="50" height="50">
                            <input name="_managers_<?php echo $key; ?>[]" type="hidden" value="<?php echo $manager[$key]; ?>">
                            <?php
                            break;
                        case 'upload_file':
                            ?>
                            <a href="#" class="button upload_file_button" data-choose="<?php _e('Choose file', 'utheme'); ?>" data-update="<?php _e('Insert file URL', 'utheme'); ?>">
                                <?php _e('Choose&nbsp;photo', 'utheme'); ?>
                            </a>
                            <?php
                            break;
                        case 'action':
                            ?>
                            <a href="#" class="delete"><?php _e('Delete', 'utheme'); ?></a>
                            <?php
                            break;
                    }
                    ?>
                </td>
                <?php
            } ?>
        </tr>
        <?php }
        } ?>
        </tbody>
    </table>
    <div class="clear"></div>

<div class="toolbar">
    <button type="button"  data-tmpl="#tmpl-strategy-manager-row"  data-container="#managers_table"  class="button add_document_row"><?php _e('Add manager', 'utheme'); ?></button>
</div>

<?php
include 'tmpl/tmpl-strategy-manager-row.php';