<div class="options_group">
    <div class="form-field">
        <label for="top_10_holdings_date"><?php _e('Holdings Date', 'utheme'); ?></label>
        <input type="text"  name="_top_10_holdings_date" id="top_10_holdings_date"  class="form-control short u-init-date" value="<?php echo  $the_strategy->top_10_holdings_date; ?>">
    </div>
    <div class="form-field">
        <label for="sector_weights_date"><?php _e('Weights Date', 'utheme'); ?></label>
        <input type="text"  name="_sector_weights_date" id="sector_weights_date"  class="form-control short u-init-date" value="<?php echo  $the_strategy->sector_weights_date; ?>">
    </div>
    <div class="form-field">
        <label for="attributes_date"><?php _e('Attributes Date', 'utheme'); ?></label>
        <input type="text"  name="_attributes_date" id="attributes_date"  class="form-control short u-init-date" value="<?php echo  $the_strategy->attributes_date; ?>">
    </div>
    <div class="form-field">
        <label for="risk_statistics_date"><?php _e('Risk statistics Date', 'utheme'); ?></label>
        <input type="text"  name="_risk_statistics" id="risk_statistics_date"  class="form-control short u-init-date" value="<?php echo  $the_strategy->risk_statistics_date; ?>">
    </div>
    <div class="form-field">
        <label for="top_countries_date"><?php _e('Top countries Date', 'utheme'); ?></label>
        <input type="text"  name="_top_countries_date" id="top_countries_date"  class="form-control short u-init-date" value="<?php echo  $the_strategy->top_countries_date; ?>">
    </div>
    <div class="clear" style="padding: 80px 0;"></div>
</div>