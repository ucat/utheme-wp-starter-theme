<?php
/**
 *
 * @class     U_ACF
 * @version   1.0.0
 * @package   U_Theme/Classes
 * @category  Class
 * @author    uCAT
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * U_Require_Plugins Class.
 */
class U_ACF
{
    public static function init_fields(){

        if(function_exists("register_field_group"))
        {
            register_field_group(array (
                'id' => 'acf_content',
                'title' => 'Content',
                'fields' => array (
                    array (
                        'key' => 'field_5af1ca175d7a3',
                        'label' => 'Sections',
                        'name' => 'u_sections',
                        'type' => 'repeater',
                        'sub_fields' => array (
                            array (
                                'key' => 'field_5af1ca495d7a4',
                                'label' => 'Background',
                                'name' => 'u_section_background',
                                'type' => 'image',
                                'column_width' => '',
                                'save_format' => 'url',
                                'preview_size' => 'medium',
                                'library' => 'all',
                            ),
                            array (
                                'key' => 'field_5af1ca7a5d7a5',
                                'label' => 'CSS ID',
                                'name' => 'u_section_css_id',
                                'type' => 'text',
                                'column_width' => '',
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'formatting' => 'html',
                                'maxlength' => '',
                            ),
                            array (
                                'key' => 'field_5af1caca5d7a6',
                                'label' => 'CSS Class',
                                'name' => 'u_section_css_class',
                                'type' => 'text',
                                'column_width' => '',
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'formatting' => 'html',
                                'maxlength' => '',
                            ),
                            array (
                                'key' => 'field_5af1cc37f4bca',
                                'label' => 'Elements',
                                'name' => 'u_section_elements',
                                'type' => 'flexible_content',
                                'column_width' => '',
                                'layouts' => array (
                                    array (
                                        'label' => 'Hero Title',
                                        'name' => 'hero-title',
                                        'display' => 'row',
                                        'min' => '',
                                        'max' => '',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_5af1cc9cf4bcb',
                                                'label' => 'Title',
                                                'name' => 'title',
                                                'type' => 'text',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'formatting' => 'html',
                                                'maxlength' => '',
                                            ),
                                            array (
                                                'key' => 'field_5af1cca3f4bcc',
                                                'label' => 'Description',
                                                'name' => 'description',
                                                'type' => 'wysiwyg',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'toolbar' => 'full',
                                                'media_upload' => 'no',
                                            ),
                                            array (
                                                'key' => 'field_5af1ccadf4bcd',
                                                'label' => 'Button Link',
                                                'name' => 'button_link',
                                                'type' => 'text',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'formatting' => 'html',
                                                'maxlength' => '',
                                            ),
                                        ),
                                    ),
                                    array (
                                        'label' => 'In Focus',
                                        'name' => 'in-focus',
                                        'display' => 'row',
                                        'min' => '',
                                        'max' => '',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_5af1d3d2a0045',
                                                'label' => 'Title',
                                                'name' => 'title',
                                                'type' => 'text',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'formatting' => 'html',
                                                'maxlength' => '',
                                            ),
                                            array (
                                                'key' => 'field_5af1d3d2a0046',
                                                'label' => 'Description',
                                                'name' => 'description',
                                                'type' => 'wysiwyg',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'toolbar' => 'full',
                                                'media_upload' => 'no',
                                            ),
                                        ),
                                    ),
                                    array (
                                        'label' => 'Strategies',
                                        'name' => 'strategies',
                                        'display' => 'row',
                                        'min' => '',
                                        'max' => '',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_5af2ca6571119',
                                                'label' => 'Title',
                                                'name' => 'title',
                                                'type' => 'text',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'formatting' => 'html',
                                                'maxlength' => '',
                                            ),
                                            array (
                                                'key' => 'field_5af2ca657111a',
                                                'label' => 'Description',
                                                'name' => 'description',
                                                'type' => 'wysiwyg',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'toolbar' => 'full',
                                                'media_upload' => 'no',
                                            ),
                                        ),
                                    ),
                                    array (
                                        'label' => 'Video',
                                        'name' => 'video',
                                        'display' => 'row',
                                        'min' => '',
                                        'max' => '',
                                        'sub_fields' => array (
                                            array (
                                                'key' => 'field_5af2cce3cfdbb',
                                                'label' => 'Title',
                                                'name' => 'title',
                                                'type' => 'text',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'formatting' => 'html',
                                                'maxlength' => '',
                                            ),
                                            array (
                                                'key' => 'field_5af2cce3cfdbc',
                                                'label' => 'Description',
                                                'name' => 'description',
                                                'type' => 'wysiwyg',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'toolbar' => 'full',
                                                'media_upload' => 'no',
                                            ),
                                            array (
                                                'key' => 'field_5af2ccedcfdbd',
                                                'label' => 'Youtube',
                                                'name' => 'youtube',
                                                'type' => 'text',
                                                'column_width' => '',
                                                'default_value' => '',
                                                'placeholder' => '',
                                                'prepend' => '',
                                                'append' => '',
                                                'formatting' => 'html',
                                                'maxlength' => '',
                                            ),
                                            array (
                                                'key' => 'field_5af2fa8d02a9a',
                                                'label' => 'Cover image',
                                                'name' => 'cover_image',
                                                'type' => 'image',
                                                'column_width' => '',
                                                'save_format' => 'url',
                                                'preview_size' => 'thumbnail',
                                                'library' => 'all',
                                            ),
                                        ),
                                    ),
                                ),
                                'button_label' => 'Add Row',
                                'min' => '',
                                'max' => '',
                            ),
                        ),
                        'row_min' => '',
                        'row_limit' => '',
                        'layout' => 'row',
                        'button_label' => 'Add Sections',
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'page_template',
                            'operator' => '==',
                            'value' => 'template-full-page.php',
                            'order_no' => 0,
                            'group_no' => 0,
                        ),
                    ),
                ),
                'options' => array (
                    'position' => 'normal',
                    'layout' => 'default',
                    'hide_on_screen' => array (
                        0 => 'the_content',
                        1 => 'excerpt',
                        2 => 'custom_fields',
                    ),
                ),
                'menu_order' => 0,
            ));
            register_field_group(array (
                'id' => 'acf_settings',
                'title' => 'Settings',
                'fields' => array (
                    array (
                        'key' => 'field_5af1c9ba3f46f',
                        'label' => 'CSS ID',
                        'name' => 'u_css_id',
                        'type' => 'text',
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'html',
                        'maxlength' => '',
                    ),
                    array (
                        'key' => 'field_5af1c9d53f470',
                        'label' => 'CSS Class',
                        'name' => 'u_css_class',
                        'type' => 'text',
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'html',
                        'maxlength' => '',
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'page',
                            'order_no' => 0,
                            'group_no' => 0,
                        ),
                    ),
                ),
                'options' => array (
                    'position' => 'normal',
                    'layout' => 'default',
                    'hide_on_screen' => array (
                    ),
                ),
                'menu_order' => 0,
            ));
        }

    }
}
U_ACF::init_fields();