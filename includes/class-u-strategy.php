<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Strategy
 *
 * These are regular strategies, which extend the abstract post class.
 *
 * @class     U_Strategy
 * @version   1.0.0
 * @package   U_Theme/Classes
 * @category  Class
 * @author    Elena Zhyvohliad
 */
class U_Strategy extends U_Abstract_Post{

    /**
     * __get function.
     *
     * @param mixed $key
     * @return mixed
     */
    public function __get( $key ) {
        // Get values or default if not set.

        $value = parent::__get( $key );

        $taxonomies = array('share_class','strategy_category','geography', 'strategy_group');

        if( in_array($key, $taxonomies) ){
            $terms = $this->get_terms( $key );
            $value = '';
            if( !is_wp_error( $terms ) && !empty($terms)){
                $value = $terms[0]->name;
            }
        }
        elseif ($key === 'category'){
            $terms = $this->get_terms( 'strategy_category' );
            $value = '';
            if( !is_wp_error( $terms ) && !empty($terms)){
                $value = $terms[0]->name;
            }
        }
        elseif ($key === 'group'){
            $terms = $this->get_terms( 'strategy_group' );
            $value = '';
            if( !is_wp_error( $terms ) && !empty($terms)){
                $value = $terms[0]->name;
            }
        }
        elseif ($key === 'title'){
            return $this->get_title();
        }

        return $value;
    }

    public function get_share_class( $obj = false ){
        $terms = $this->get_terms('share_class');
        if( $obj ){
            return $terms;
        }else if( !is_wp_error( $terms ) && !empty($terms)){
            return $terms[0]->name;
        }
    }



	/**
	 * Init/load the strategy object. Called from the constructor.
	 *
	 * @param  int|object|U_Strategy $strategy Strategy to init.
	 */
	protected function init( $strategy ) {
        if ( $strategy instanceof U_Strategy ) {
            $this->id   = absint( $strategy->id );
            $this->post = $strategy->post;
            $this->get_post( $this->id );
        }else{
            parent::init( $strategy );
        }
	}

	public function add_ticker( $data = array() )
	{
		$args = array(
			'post_type'     => 'ticker',
			'post_status'   => 'publish',
			'ping_status'   => 'closed',
			'post_author'   => 1,
			'post_password' => uniqid( 'strategy_ticker_' ),
			'post_title'    => sprintf( __( 'Ticker &ndash; %s', 'utheme' ), strftime( _x( '%b %d, %Y @ %I:%M %p', 'Ticker date parsed by strftime', 'utheme' ) ) ),
			'post_parent'   => $this->id
		);
		$ticker_id = wp_insert_post( $args, true );
		if ( is_wp_error( $ticker_id ) ) {
			return $ticker_id;
		}

		update_post_meta( $ticker_id, '_utheme_version', U_THEME_VERSION );
		return $ticker_id;
	}


	public function get_documents_list(){
        return [
            'prospectus'       => __( 'Prospectus', 'utheme' ),
            'fact_sheet'       => __( 'Fact Sheet', 'utheme' ),
            'cap_gains'        => __( 'Cap Gains', 'utheme' ),
        ];
    }
	public function get_document_url($id)
	{
        $docs = $this->get_documents_list();
        if( isset($docs[$id]) ){
            return $this->{'document_'. $id };
        }else{
            $additional_doc = $this->additional_doc;
            if (isset($additional_doc[$id])){
                return $additional_doc[$id]['url'];
            }
        }
        return false;
	}

    public function get_document_name($id)
    {
        $docs = $this->get_documents_list();
        if( isset($docs[$id]) ){
            return $docs[$id];
        }else{
            $additional_doc = $this->additional_doc;
            if (isset($additional_doc[$id])){
                return $additional_doc[$id]['name'];
            }
        }
        return false;
    }
    public function get_managers()
    {
        return $this->managers;
    }

}
