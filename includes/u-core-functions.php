<?php
/**
 * Theme Core Functions
 *
 * General core functions available on both the front-end and admin.
 *
 * @author 		uCAT
 * @category 	Core
 * @package 	U_Theme/Functions
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Include core functions (available in both admin and frontend).
include( 'u-strategy-functions.php' );
include( 'u-people-functions.php' );
include( 'u-frontend-functions.php' );



if ( ! function_exists( 'is_ajax' ) ) {

	/**
	 * is_ajax - Returns true when the page is loaded via ajax.
	 * @return bool
	 * @since  1.0.1
	 */
	function is_ajax() {
		return defined( 'DOING_AJAX' );
	}
}

/**
 * Queue some JavaScript code to be output in the footer.
 *
 * @param string $code
 */
function u_enqueue_js( $code ) {
    global $u_queued_js;

    if ( empty( $u_queued_js ) ) {
        $u_queued_js = '';
    }

    $u_queued_js .= "\n" . $code . "\n";
}

/**
 * Output any queued javascript code in the footer.
 */
function u_print_js() {
    global $u_queued_js;

    if ( ! empty( $u_queued_js ) ) {
        // Sanitize.
        $u_queued_js = wp_check_invalid_utf8( $u_queued_js );
        $u_queued_js = preg_replace( '/&#(x)?0*(?(1)27|39);?/i', "'", $u_queued_js );
        $u_queued_js = str_replace( "\r", '', $u_queued_js );

        $js = "<!-- U_THEME JavaScript -->\n<script type=\"text/javascript\">\njQuery(function($) { $u_queued_js });\n</script>\n";

        /**
         * utheme_queued_js filter.
         *
         * @param string $js JavaScript code.
         */
        echo apply_filters( 'utheme_queued_js', $js );

        unset( $u_queued_js );
    }
}

/**
 * Checks to see if we're on the homepage or not.
 */
function u_is_frontpage() {
    return ( is_front_page() && ! is_home() );
}

function u_get_assets_uri($file = ''){
     return get_theme_file_uri('/assets/' . $file) ;
}

if ( ! function_exists( 'u_edit_link' ) ) :
    /**
     * Returns an accessibility-friendly link to edit a post or page.
     *
     * This also gives us a little context about what exactly we're editing
     * (post or page?) so that users understand a bit more where they are in terms
     * of the template hierarchy and their content. Helpful when/if the single-page
     * layout with multiple posts/pages shown gets confusing.
     */
    function u_edit_link() {
        edit_post_link(
            sprintf(
            /* translators: %s: Name of current post */
                __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'utheme' ),
                get_the_title()
            ),
            '<span class="edit-link">',
            '</span>'
        );
    }
endif;

/**
 * Clean variables using sanitize_text_field. Arrays are cleaned recursively.
 * Non-scalar values are ignored.
 * @param string|array $var
 * @return string|array
 */
function u_clean( $var ) {
    if ( is_array( $var ) ) {
        return array_map( 'u_clean', $var );
    } else {
        return is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
    }
}
function u_get_container_id(){
    $id = get_field('u_css_id');
    if( !empty($id)) return $id;

    if( get_page_template_slug() === 'template-full-page.php' || is_single() ){
        return 'fullpage';
    }
    return 'main';
}

function u_get_container_class(){
    $class[] = get_field('u_css_class');

    if( is_single() || is_post_type_archive('strategy') ){
        $class[] = 'no-autoscroll';
    }
    if(is_singular('strategy') ){
        $class[] = 'fullpage-wrapper';
    }
    return implode(' ', $class);
}

function u_number_format($v, $precision = 2){
    return number_format( floatval($v), $precision);
}
function u_price_format($v, $precision = 2){
    return '$'. u_get_short_number_format($v, $precision);
}

function u_percent_format($v, $precision = 2){
    $n_format = number_format( floatval($v), $precision);
    if ( $precision > 0 ) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }
    return $n_format . '%';
}

/**
 * @param $n
 * @return string
 * Use to convert large positive numbers in to short form like 1K+, 100K+, 199K+, 1M+, 10M+, 1B+ etc
 */
function u_get_short_number_format( $n , $precision = 2, $long = false) {
    $n = floatval($n);
    if ($n < 900) {
        // 0 - 900
        $n_format = number_format($n, $precision);
        $suffix = '';
    } else if ($n < 900000) {
        // 0.9k-850k
        $n_format = number_format($n / 1000, $precision);
        $suffix = 'KK';
        if( $precision == 1 ){
            $n_format = array_shift(array_values( explode('.', $n_format) ));
        }
    } else if ($n < 900000000) {
        // 0.9m-850m
        $n_format = number_format($n / 1000000, $precision);
        $suffix = 'MM';
    } else if ($n < 900000000000) {
        // 0.9b-850b
        $n_format = number_format($n / 1000000000, $precision);
        $suffix = 'BB';
    } else {
        // 0.9t+
        $n_format = number_format($n / 1000000000000, $precision);
        $suffix = 'TT';
    }
    // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
    // Intentionally does not affect partials, eg "1.50" -> "1.50"
    if ( $precision > 0 && $n > 999) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }
    return $n_format . ' ' . $suffix;
}