/*!
 * uYoutubeApi 0.0.1
 * https://github.com/crazysnowflake/uYoutubeApi.jq
 * @version 0.0.1
 * @license MIT licensed
 *
 * Copyright (C) 2018 ucat.biz- A project by Elena Zhyvohliad
 */
(function($) {
    'use strict';

    $.fn.uYoutubeApi = function( options  ) {

        var $window = $(window);
        var $document = $(document);

        var opt = $.extend({
            playbtn: '.play-btn'
        }, options);


        this.each(function(e, i){
            var $el = $(this);

            var app = {
                id   : '',
                url  : '',
                uid  : '',
                player : null,
                done   : false,
                init   : function( options ) {
                    app.id  = app.id = makeid();
                    app.url = $el.data('url') != undefined ? $el.data('url') : '';

                    if( app.url === '' ){
                        return;
                    }

                    app.uid = YouTubeGetID(app.url);

                    $el.wrapInner('<div id="u-yapi-holder-'+app.id+'" class="u-yapi-holder"></div>');
                    $el.append('<div id="'+app.id+'" class="u-yapi-player" style="display: none; width: 100%; height: 100%; position: absolute; top: 0;" ></div>');

                    var $playbtn = $el.find(opt.playbtn);
                    $el.on('click', $playbtn, app.playVideo);

                },
                apiReady : function( ) {
                    app.player = new YT.Player(app.id, {
                        height : '100%',
                        width  : '100%',
                        videoId: app.uid
                        /*events: {
                            onApiChange: function(event){
                                console.log(event);
                                //console.log(app);
                                //app.onPlayerStateChange(event);
                            },
                            onStateChange: function(event){
                                console.log(event);
                                //console.log(app);
                                //app.onPlayerStateChange(event);
                            }
                        }*/
                    });
                },
                onPlayerStateChange : function(event) {

                    /*if (event.data == YT.PlayerState.PLAYING && !app.done) {
                        setTimeout(app.stopVideo, 6000);
                        app.done = true;
                    }*/
                },
                stopVideo : function() {
                    var $wraper = $(this).closest('.uYoutubeApi-init');
                    if( !$wraper.length ) return false;

                    var $holder  = $wraper.find('#u-yapi-holder-'+app.id);
                    var $player  = $wraper.find('#'+app.id);

                    //$holder.show();
                    $player.hide();


                    app.player.playVideo();
                    return false;
                },
                playVideo : function() {
                    var $wraper = $(this).closest('.uYoutubeApi-init');
                    if( !$wraper.length ) return false;

                    var $holder  = $wraper.find('#u-yapi-holder-'+app.id);
                    var $player  = $wraper.find('#'+app.id);

                    //$holder.hide();
                    $player.show();


                    app.player.playVideo();
                    return false;
                }
            };

            app.init();

            $(this).data('uYoutubeApi', app).addClass('uYoutubeApi-init');

        });

        //return this;
    };

    function makeid() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 15; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    function YouTubeGetID(url){
        var ID = '';
        url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
        if(url[2] !== undefined) {
            ID = url[2].split(/[^0-9a-z_\-]/i);
            ID = ID[0];
        }
        else {
            ID = url;
        }
        return ID;
    }




    var tag = document.createElement('script');
    tag.id = 'iframe-uYoutubeApi';
    tag.src = "https://www.youtube.com/iframe_api";

    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    window.onYouTubeIframeAPIReady = function(){

        $('.uYoutubeApi-init').each(function(){
            var _app = $(this).data('uYoutubeApi');
            _app.apiReady();
        });

    };

}(jQuery));
jQuery(function() {
    jQuery('.u-yapi').uYoutubeApi({
        imgholder: '.img-holder',
        playbtn: '.play-btn'
    });
});

