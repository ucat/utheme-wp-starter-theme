(function($) {
    'use strict';

    $(function() {

        var frontapp = {
            init : function () {
                $(document)
                    .on('click', '.change-qtr-mth', this.changeQtrMth)

            },
            changeQtrMth : function (e) {
                var type = $(this).data('type');
                switch (type) {
                    case 'monthly':
                        $('.strtg-mth-data').show();
                        $('.strtg-qtr-data').hide();
                        break;
                    case 'quarterly':
                        $('.strtg-mth-data').hide();
                        $('.strtg-qtr-data').show();
                        break
                }

                return false;
            }
        };

        frontapp.init();

    });

}(jQuery));


