/*!
 * uYoutubeApi 0.0.1
 * https://github.com/crazysnowflake/u-bulder
 * @version 0.0.1
 * @license MIT licensed
 *
 * Copyright (C) 2018 ucat.biz- A project by Elena Zhyvohliad
 */
(function($) {
    'use strict';

    $.fn.tagNameLowerCase = function() {
        return this.prop("tagName").toLowerCase();
    };

    $.fn.tagName = function() {
        return this.prop("tagName");
    };
    $.fn.setUID = function(id) {
        return this.attr('u-id', id);
    };

    $.fn.removeAttributes = function(only, except) {
        if (only) {
            only = $.map(only, function(item) {
                return item.toString().toLowerCase();
            });
        };
        if (except) {
            except = $.map(except, function(item) {
                return item.toString().toLowerCase();
            });
            if (only) {
                only = $.grep(only, function(item, index) {
                    return $.inArray(item, except) == -1;
                });
            };
        };
        return this.each(function() {
            var attributes;
            if(!only){
                attributes = $.map(this.attributes, function(item) {
                    return item.name.toString().toLowerCase();
                });
                if (except) {
                    attributes = $.grep(attributes, function(item, index) {
                        return $.inArray(item, except) == -1;
                    });
                };
            } else {
                attributes = only;
            }
            var handle = $(this);
            $.each(attributes, function(index, item) {
                handle.removeAttr(item);
            });
        });
    };

    $.fn.uBuilder = function( options  ) {

        var $window = $(window);
        var $document = $(document);

        var app = {
            appid      : 'u-app',
            wrapper    : false,
            activeID   : 0,
            init : function () {
                this.wrapper = $(app.appid);
                if( this.wrapper.length > 1 ){
                    alert('You cant have more then one application');
                    return false;
                }
                this.wrapper.setUID(this.activeID);
                if( this.getItem('UDOM') === null ){
                    this.saveItem(this.activeID);
                    this.setItem('UDOM', {});
                }else{

                }
                this.renderMarkup();

                this.selectElement(this.activeID);
                $('u-app-sidebar').addClass('open');

                $(document)
                    .on('click', '#u-add-tag', app.addTag)
                    .on('click', '#u-remove-tag', app.removeTag)
                    .on('click', '#u-add-attr-row', app.addAttrRow)
                    .on('click', '.u-remove-attr', app.removeAttrRow)
                    .on('change', '#u-tag-name', app.setTagName)
                    .on('u-after-remove-attribute', app.setAttr)
                    .on('change', '.u-tag-att', app.setAttr);

            },
            initUI : function () {
                $( ".u-nav a" ).click(function (e) {
                    $('li', $(this).closest('ul')).removeClass('active').each(function (i,e) {
                        var id = $('a', this).attr('href');
                        $(id).removeClass('active').removeClass('in');
                    });
                    $(this).closest('li').addClass('active');
                    var id = $(this).attr('href');
                    $(id).addClass('active in');
                });
            },

            /**
             * Event handling
             * */
            addTag : function (e) {
                var id     = Date.now();
                var $newEl = app.renderElement({id: id, tag: 'div'});

                app.getActiveElement().append($newEl);

                app.saveItem(id);
                app.addDOMItem( app.activeID, id );

                app.selectElement(id);
                return false;
            },
            removeTag : function (e) {
                app.getElement(app.activeID).remove();
                var parent = app.getParentItem(app.activeID);
            },

            setTagName : function () {
                var tagcorrect = true,
                    replaced   = false,
                    $element   = app.getActiveElement(),
                    item       = app.getActiveItem(),
                    tagname    = $(this).val();
                if( tagname === '' ){
                    tagname = 'div';
                }
                tagname = tagname.trim().toLowerCase().replace(/\s/g,'');
                if( tagname === item.tag ) return false;

                if(tagcorrect){
                    item.tag = tagname;
                    app.saveItem(app.activeID, item);

                    $element.replaceWith('<'+tagname+' u-id="'+app.activeID+'">' + $element.html() +'</'+tagname+'>');
                    replaced = true;
                }else if (confirm('Do you shure want delete inner content?')){
                    $element.replaceWith('<'+tagname+'/>');
                    replaced = true;
                }

                if( replaced ){
                    app.setAttributes(app.activeID);
                    app.renderSidebar();
                }

                return false;
            },
            setAttr : function () {
                var $element   = app.getActiveElement(),
                    item       = app.getActiveItem(),
                    attributes = [];

                $('#u-general-attributes .u-tag-att').each(function (i, e) {
                    var name = '', val ='';
                    name = $(e).attr('u-attr');
                    val  = $(e).val();

                    if (val != ''){
                        attributes.push({
                            'name'  : name,
                            'value' : val
                        });
                    }

                });
                $('#u-additional-attributes .u-att-group').each(function (i, e) {
                    var name = '', val ='';
                    name = $(e).find('.u-att-name').val();
                    val  = $(e).find('.u-att-val').val();

                    if (name != ''){
                        attributes.push({
                            'name'  : name,
                            'value' : val
                        });
                    }
                });

                item.attributes = attributes;
                app.saveItem(app.activeID, item);
                app.setAttributes(app.activeID);
            },
            addAttrRow : function (e) {
                var tmpl = $('#utmpl-additiona-attribute-row').html();
                $('#u-additional-attributes').append(tmpl);
                return false;
            },
            removeAttrRow : function (e) {
                $(this).closest('.u-att-group').remove();
                $(document).trigger('u-after-remove-attribute');
                return false;
            },

            /**
             * Helper methods
             * */
            getElement : function (id) {
                return $('[u-id=' + id + ']');
            },
            getActiveElement : function () {
                return app.getElement(app.activeID);
            },
            selectElement : function (id) {
                app.activeID = id;
                app.renderSidebar();
            },
            getChildren : function (id) {
                return app.getElement(id).children();
            },
            hasChild : function (id) {
                return !(!app.getChildren(id).length);
            },
            activeHasChild : function () {
                return app.hasChild(app.activeID);
            },


            /**
             * localStorage methods
             * */
            setItem : function (key, data) {
                data = JSON.stringify(data);
                localStorage.setItem(key, data);
            },
            saveItem : function (id, data) {
                if( typeof data === 'undefined' ){
                    data = {
                        id : '',
                        tag : ''
                    };
                }
                data['id'] = id;
                if( typeof data.tag === 'undefined' || data.tag === ''){
                    data['tag'] = app.getElement(id).tagNameLowerCase();
                }
                if( typeof data.attributes === 'undefined' ){
                    data['attributes'] = [];
                }
                data = JSON.stringify(data);
                localStorage.setItem(id, data);
            },
            getItem : function (id) {
                var item = localStorage.getItem(id);
                return JSON.parse(item);
            },
            getActiveItem : function () {
                return app.getItem(app.activeID);
            },
            getParentItem : function (id, all) {
                var item = 0;
                if( all === 'all'){

                }else{

                }
                return item;
            },
            removeItem : function (id) {
                if( app.activeID === id ){
                    app.selectElement(app.getParentItem(id));
                }
                localStorage.removeItem(id);
            },
            clearWorkspace : function (id) {
                app.activeID = 0;
                localStorage.clear();
                app.renderSidebar();
                app.renderMarkup();
            },
            getDOM : function () {
                var dom = this.getItem('UDOM');
                if( dom === null ){
                    dom = {};
                }
                return dom;
            },
            addDOMItem : function (parent, id) {
                var dom = app.getDOM();
                console.log(parent);
                if( parent === 0){
                    dom[id] = {};
                }else{
                    dom = app.__addDOMitem(dom, parent, id);
                }
                this.setItem('UDOM', dom);
            },

            __addDOMitem : function (data, parent, id) {
                var items = data;
                $.each(data, function (key, value) {
                    key = parseInt(key);
                    if( key === parent ){
                        items[key][id] = {};
                        return;
                    }else if( !jQuery.isEmptyObject(value) ){
                        items[key] = app.__addDOMitem(data[key], parent, id);
                    }
                });
                return items;
            },

            /**
             * Manipulating DOM elements
             * */
            setAttributes : function (id) {
                var $element   = app.getElement(id),
                    item       = app.getActiveItem();

                $element.removeAttributes(null, ['u-id']);

                $.each(item.attributes, function (i, a) {
                    $element.attr(a.name, a.value);
                });
            },

            /**
             * Render methods
             * */
            renderSidebar : function () {

            },
            buildDOM : function () {
                var $ul = $('</ul>');
                $('#u-tab-dom').html('<ul id="u-dom-list"></ul>');
                var dom  = app.getDOM();
                app.__buildDOM(dom, $('#u-dom-list'));
            },
            __buildDOM : function (dom, $parent) {
                $.each(dom, function (id, children) {

                    var item     = app.getItem(id),
                        $element = $('<li>', {'u-elid' : item.id}),
                        text     = '<i class="u-dom-el-tag">' + item.tag + '</i>';
                    var tid = '', tclass = '';

                    if(typeof item.attributes !== 'undefined' ){
                        $.each(item.attributes, function (i, a) {
                            if (a.value === '') return;
                            if( a.name == 'id' ){
                                tid = '<i class="u-dom-el-id">#' + a.value + '</i>';
                            }
                            if( a.name == 'class'){
                                tclass = '.' + a.value.split(' ').join('.');
                                tclass = '<i class="u-dom-el-class">' + tclass + '</i>';
                            }
                        });
                    }
                    text += tid+tclass;
                    $element.html(text);
                    $parent.append($element);
                    if( !jQuery.isEmptyObject(children) ){
                        $element = $('[u-elid=' + item.id + ']', $parent);
                        app.__buildDOM(children, $element);
                    }
                });
                return $parent;
            },
            renderMarkup : function () {
                var dom  = app.getDOM();
                app.__renderMarkup(dom, 0);
                app.buildDOM();
            },
            __renderMarkup : function ( dom, parent ) {
                var $parent = app.getElement(parent);
                $.each(dom, function (id, children) {

                    var item     = app.getItem(id),
                        $element = app.renderElement(item);
                    $parent.append($element);
                    if( !jQuery.isEmptyObject(children) ){

                        app.__renderMarkup(children, id);
                    }
                });
                return $parent;
            },
            renderElement : function(item){
                var item = $.extend({
                    id  : '',
                    tag : 'div'
                }, item);

                if( item.id === '') return '';

                var attributes = {
                    //text: 'Go to Google!'
                };

                $.each(item.attributes, function (i, a) {
                    attributes[a.name] = a.value;
                });

                return $('<' + item.tag + '/>', attributes).setUID(item.id);
            }

        };

        app.init();
        app.initUI();

    };

}(jQuery));
jQuery(function() {
    jQuery(document).uBuilder();
});

