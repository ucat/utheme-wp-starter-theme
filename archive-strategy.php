<?php
/**
 * Strategies Archive
 *
 * @package		U_Theme/Template
 * @author 		uCAT
 */

$columns = array(
    array(
        'items' => array(
            array(
                'type'  => 'title',
                'title' => __('Product Name', 'utheme'),
                'key'   => 'title',
                'class' => 'td-group-right max-desktop no-sort'
            )
        )
    ),
    array(
        'items' => array(
            array(
                'type'   => 'ticker',
                'title'  => __('Ticker', 'utheme'),
                'key'    => 'ticker',
                'class'  => 'td-group-left min-tablet no-sort'
            ),
            array(
                'title' => __('Category', 'utheme'),
                'key'   => 'category',
                'class'   => 'td-group-right min-tablet no-sort'
            )
        )
    ),
    'downlods' => array(
        'items' => array(
            array(
                'type'   => 'download',
                'title'  => __('Fact Sheet', 'utheme'),
                'key'    => 'fact_sheet',
                'class'  => 'td-group-left min-tablet no-sort'
            ),
            array(
                'type'   => 'download',
                'title' => __('Commentary', 'utheme'),
                'key'   => 'commentary',
                'class'   => 'min-tablet no-sort'
            ),
            array(
                'type'   => 'download',
                'title' => __('Cap Gains & Income', 'utheme'),
                'key'   => 'cap_gains',
                'class'   => 'td-group-right min-tablet no-sort'
            )
        )
    ),
    'daily_price' => array(
        'title'    => __('Daily Price & YTD Return', 'utheme'),
        'subtitle' => __('as of 00/00/00', 'utheme'),
        'items'    => array(
            array(
                'title'    => __('NAV($)', 'utheme'),
                'key'      => 'monthly_nav',
                'class'    => 'td-group-left min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('CHG($)', 'utheme'),
                'key'   => 'monthly_chg',
                'class' => 'min-tablet no-sort',
                'callback' => 'u_number_format',
            ),
            array(
                'title' => __('YTD(%)', 'utheme'),
                'key'   => 'monthly_ytd_return',
                'class'   => 'td-group-right min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
        )
    ),
    'annual_total' => array(
        'title'    => __('Average Annual Total Returns (%)', 'utheme'),
        'subtitle' => __('as of 00/00/00', 'utheme'),
        'items'    => array(
            array(
                'title' => __('QTR', 'utheme'),
                'key'   => 'qtr',
                'class'   => 'td-group-left min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('YTD', 'utheme'),
                'key'   => 'ytd',
                'class'   => 'min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('1 YR', 'utheme'),
                'key'   => '1yr',
                'class'   => 'min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('3 YR', 'utheme'),
                'key'   => '3yr',
                'class'   => 'min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('5 YR', 'utheme'),
                'key'   => '5yr',
                'class'   => 'min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('10 YR', 'utheme'),
                'key'   => '10yr',
                'class'   => 'min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('15 YR', 'utheme'),
                'key'   => '15yr',
                'class'   => 'min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('Incpt.', 'utheme'),
                'key'   => 'incpt',
                'class'   => 'min-tablet no-sort',
                'callback' => 'u_number_format'
            ),
            array(
                'title' => __('Date', 'utheme'),
                'key'   => 'inception_date',
                'class'   => 'min-tablet no-sort'
            ),
            array(
                'type'    => 'href',
                'title'   => '',
                'key'     => '',
                'class'   => 'min-tablet no-sort display-xs-visible'
            ),
        )
    )
);
$groups = u_get_strategy_groups(true);
get_header(); ?>
<div id="main" class="no-autoscroll">
    <section class="section-strategies">
        <div class="tabset-holder">
            <ul class="tabset text-uppercase">
                <?php $i = 0; foreach ( u_get_strategy_types() as $type_key => $type_name ): ?>
                    <li><a href="#tab-<?php echo $type_key; ?>" <?php $i === 0 ? 'class="active"' : ''; ?>><?php echo $type_name; ?></a></li>
                    <?php $i++; endforeach; ?>
            </ul>
        </div>
        <div class="tab-content">
            <?php foreach ( u_get_strategy_types() as $type_key => $type_name ): ?>

            <div id="tab-<?php echo $type_key; ?>">
                <div class="tab-holder">
                    <div class="text-title-wrap">
                        <div class="container">
                            <header class="section-header">
                                <h1 class="title text-uppercase"><?php _e('Strategies', 'utheme'); ?></h1>
                            </header>
                            <div class="text-holder">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            </div>
                        </div>
                    </div>
                    <div class="table-wrap">
                        <div class="container-fluid">
                            <div class="scroll-holder">
                                <div class="scroll-wrap">
                                    <ul class="tabset-table text-uppercase display-xs-hidden">
                                        <li><a href="#" data-type="monthly" class="active change-qtr-mth">Monthly</a></li>
                                        <li><a href="#" data-type="quarterly" class="change-qtr-mth">Quarterly</a></li>
                                    </ul>
                                    <div class="tab-content">

                                        <div id="table-аа">
                                            <table class="responsive-table filter-checkbox-table table table-sticky mod">
                                                <thead>
                                                <tr>
                                                    <?php
                                                    foreach ($columns as $column_gr_key => $column_group ){
                                                    $colspan = count($column_group['items']);
                                                    ?>
                                                    <th <?php echo $colspan > 1 ? 'colspan="'.$colspan.'"' : ''; ?> class="td-group min-desktop no-sort text-left">
                                                        <div class="title"><?php echo isset( $column_group['title'] ) ? $column_group['title'] : ''; ?></div>
                                                        <div class="subtitle">
                                                            <?php echo isset( $column_group['subtitle'] ) ? $column_group['subtitle'] : ''; ?>
                                                        </div>
                                                    </th>
                                                    <?php } ?>
                                                </tr>
                                                <tr>
                                                    <?php
                                                    foreach ($columns as $column_gr_key => $column_group ){
                                                        foreach ($column_group['items'] as $column ){
                                                        $class = isset($column['class']) ? $column['class'] : 'td-group';
                                                        ?>
                                                        <th class="<?php echo $class; ?>">
                                                        <?php
                                                            if($column_gr_key == 'annual_total' && $column['key'] == 'qtr'){
                                                                ?>
                                                                <span class="strtg-mth-data"><?php _e('MTH', 'utheme'); ?></span>
                                                                <span class="strtg-qtr-data"><?php echo $column['title']; ?></span>
                                                                <?php
                                                            }else{
                                                                echo $column['title'];
                                                            }
                                                        ?>
                                                        </th>
                                                        <?php
                                                        }
                                                    } ?>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php if( $groups ){
                                                    foreach ($groups as $group_id => $group_title ){
                                                        $strategies = u_get_strategies( array('type' => $type_key, 'group' => $group_id ) );
                                                        if( !$strategies ) continue;
                                                        $strategy = $strategies[0];
                                                        ?>
                                                    <tr>
                                                    <?php
                                                    $i = 0; foreach ($columns as $column_gr_key => $column_group ){
                                                        foreach ($column_group['items'] as $column ){
                                                            $class      = isset($column['class']) ? $column['class'] : 'td-group';
                                                            $columntype = isset($column['type']) ? $column['type'] : '';
                                                            ?>
                                                            <td class="<?php echo $class; ?>">
                                                                <?php if( $columntype === 'title' ){ ?>
                                                                <div class="mark-royal-blue">
                                                                    <a href="<?php echo $strategy->get_permalink(); ?>"><?php echo $group_title; ?></a>
                                                                </div>
                                                                <?php }elseif( $columntype === 'ticker' ) {

                                                                    if( count($strategies) > 1 ){ ?>
                                                                    <div class="sort-form">
                                                                        <span class="fake-select-holder">
                                                                            <select class="fake-select">
                                                                                <?php foreach ($strategies as $strtg){ ?>
                                                                                <option value="<?php echo $strtg->id; ?>"><?php echo $strtg->ticker; ?></option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </span>
                                                                    </div>
                                                                    <?php }else{
                                                                        echo $strategy->ticker;
                                                                    }

                                                                }elseif( $columntype === 'download' ) { ?>
                                                                    <a download=""><i class="icon-download"></i>Download</a>
                                                                <?php
                                                                } elseif( $columntype === 'href') { ?>
                                                                <a href="<?php echo $strategy->get_permalink(); ?>" class="btn btn-secondary text-uppercase">Learn More</a>
                                                                <?php
                                                                } elseif ( $column_gr_key == 'annual_total' && $column['key'] != 'inception_date' ){
                                                                    $m_key = 'monthly_' . $column['key'];
                                                                    $q_key = 'quarterly_' . $column['key'];
                                                                    $m_val = $strategy->{$m_key};
                                                                    $q_val = $strategy->{$q_key};
                                                                    if( isset($column['callback']) ){
                                                                        $m_val = call_user_func($column['callback'], $m_val);
                                                                        $q_val = call_user_func($column['callback'], $q_val);
                                                                    }
                                                                    ?>
                                                                    <span class="strtg-mth-data"><?php echo $m_val; ?></span>
                                                                    <span class="strtg-qtr-data"><?php echo $q_val; ?></span>
                                                                    <?php
                                                                }else{
                                                                    if( isset($column['callback']) ){
                                                                        echo call_user_func($column['callback'], $strategy->{$column['key']});;
                                                                    }else{
                                                                        echo $strategy->{$column['key']};
                                                                    }
                                                                ?>
                                                            </td>
                                                            <?php
                                                            }
                                                        }
                                                    } ?>
                                                    </tr>
                                                <?php }
                                                } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-box-wrap">
                        <div class="container">
                            <div class="collapse-box">
                                <div class="title text-uppercase">Disclosures</div>
                                <div class="collapse-holder">
                                    <div class="text-box collapse">
                                        <?php echo apply_filters( 'the_content', get_option('u_strategy_disclosures') ); ?>
                                    </div>
                                    <a class="collapse-btn btn btn-secondary">
                                        <i class="icon-arrow-down"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php endforeach; ?>

        </div>
    </section>
</div>
<?php get_footer();
